<?php

namespace ez\widgets;

use yii\base\InvalidConfigException;
use yii\bootstrap\Dropdown;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Base on yii\boostrap\Tabs.
 * Only add one param for an item to render a header with another link
 * instead of "#".
 * And, another param for header.
 *
 * Addition params: `string url`, `array header`.
 *
 * For example:
 *
 * ```php
 * echo Tabs::widget([
 *     'header' => [
 *         'label' => '<i class="fa fa-th"> Header</i>',
 *         'options' => [
 *             'class' => 'class' => 'header',
 *         ],
 *     ],
 *     'items' => [
 *         [
 *             'label' => Html::a('One', ['index', 'one']),
 *             'url' => Url::toRoute(['index', 'action']),
 *             'content' => 'Anim pariatur cliche...',
 *             'active' => true
 *         ],
 *         [
 *             'label' => Html::a('Two', ['index', 'tow']),
 *             'url' => Url::toRoute(['index', 'action']),
 *             'content' => 'Anim pariatur cliche...',
 *             'headerOptions' => [...],
 *             'options' => ['id' => 'myveryownID'],
 *         ],
 *         [
 *             'label' => 'Dropdown',
 *             'items' => [
 *                  [
 *                      'label' => 'DropdownA',
 *                      'content' => 'DropdownA, Anim pariatur cliche...',
 *                  ],
 *                  [
 *                      'label' => 'DropdownB',
 *                      'content' => 'DropdownB, Anim pariatur cliche...',
 *                  ],
 *             ],
 *         ],
 *     ],
 * ]);
 * ```
 *
 * @author Thanh Vinh <thanh.vinh@hotmail.com>
 * @since 1.0
 */
class Tabs extends \yii\bootstrap\Tabs
{
    /**
     * @var array  Customize header
     */
    public $header;

    /**
     * Renders tab items as specified on [[items]].
     * @return string the rendering result.
     * @throws InvalidConfigException.
     */
    protected function renderItems()
    {
        $headers = [];
        $panes = [];

        // Customize header
        if (isset($this->header)) {
            $headers[] = Html::tag('li', $this->header['label'], $this->header['options']);
        }

        if (!$this->hasActiveTab() && !empty($this->items)) {
            $this->items[0]['active'] = true;
        }

        foreach ($this->items as $n => $item) {
            if (!array_key_exists('label', $item)) {
                throw new InvalidConfigException("The 'label' option is required.");
            }
            $encodeLabel = isset($item['encode']) ? $item['encode'] : $this->encodeLabels;
            $label = $encodeLabel ? Html::encode($item['label']) : $item['label'];
            $headerOptions = array_merge($this->headerOptions, ArrayHelper::getValue($item, 'headerOptions', []));
            $linkOptions = array_merge($this->linkOptions, ArrayHelper::getValue($item, 'linkOptions', []));

            if (isset($item['items'])) {
                $label .= ' <b class="caret"></b>';
                Html::addCssClass($headerOptions, 'dropdown');

                if ($this->renderDropdown($n, $item['items'], $panes)) {
                    Html::addCssClass($headerOptions, 'active');
                }

                Html::addCssClass($linkOptions, 'dropdown-toggle');
                $linkOptions['data-toggle'] = 'dropdown';

                if (!isset($item['items']['url'])) {
                    $header = Html::a($label, "#", $linkOptions) . "\n";
                } else {
                    $header = Html::a($label, $item['items']['url'], $linkOptions) . "\n";
                }
                $header .= Dropdown::widget(['items' => $item['items'], 'clientOptions' => false, 'view' => $this->getView()]);
            } else {
                $options = array_merge($this->itemOptions, ArrayHelper::getValue($item, 'options', []));
                $options['id'] = ArrayHelper::getValue($options, 'id', $this->options['id'] . '-tab' . $n);

                Html::addCssClass($options, 'tab-pane');
                if (ArrayHelper::remove($item, 'active')) {
                    Html::addCssClass($options, 'active');
                    Html::addCssClass($headerOptions, 'active');
                }

                if (!isset($item['url'])) {
                    $linkOptions['data-toggle'] = 'tab';
                    $header = Html::a($label, '#' . $options['id'], $linkOptions);
                } else {
                    $header = Html::a($label, $item['url'], $linkOptions);
                }

                if ($this->renderTabContent) {
                    $panes[] = Html::tag('div', isset($item['content']) ? $item['content'] : '', $options);
                }
            }

            $headers[] = Html::tag('li', $header, $headerOptions);
        }

        return Html::tag('ul', implode("\n", $headers), $this->options)
            . ($this->renderTabContent ? "\n" . Html::tag('div', implode("\n", $panes), ['class' => 'tab-content']) : '');
    }

    /**
     * Normalizes dropdown item options by removing tab specific keys `content` and `contentOptions`, and also
     * configure `panes` accordingly.
     * @param string $itemNumber number of the item
     * @param array $items the dropdown items configuration.
     * @param array $panes the panes reference array.
     * @return boolean whether any of the dropdown items is `active` or not.
     * @throws InvalidConfigException
     */
    protected function renderDropdown($itemNumber, &$items, &$panes)
    {
        $itemActive = false;

        foreach ($items as $n => &$item) {
            if (is_string($item)) {
                continue;
            }
            if (!array_key_exists('content', $item)) {
                if (!isset($item['url']))
                    throw new InvalidConfigException("The 'content' option is required.");
            }

            $options = ArrayHelper::remove($item, 'contentOptions', []);
            Html::addCssClass($options, 'tab-pane');
            if (ArrayHelper::remove($item, 'active')) {
                Html::addCssClass($options, 'active');
                Html::addCssClass($item['options'], 'active');
                $itemActive = true;
            }

            $options['id'] = ArrayHelper::getValue($options, 'id', $this->options['id'] . '-dd' . $itemNumber . '-tab' . $n);

            if (!isset($item['url'])) {
                $item['url'] = '#' . $options['id'];
                $item['linkOptions']['data-toggle'] = 'tab';
            }

            if (!isset($item['url'])) {
                $content = ArrayHelper::remove($item, 'content');
                $panes[] = Html::tag('div', $content, $options);
            }

            unset($item);
        }

        return $itemActive;
    }
}
