<?php

namespace ez\widgets;

use yii\base\InvalidConfigException;
use yii\helpers\Html;
use yii\widgets\InputWidget;

/**
 * Live search in a page content.
 * Usage:
 * 	<?= SwitchButton::widget([
 * 		'checked' => false,
 * 		'clientOptions' => [
 * 			'id' => 'switch-button-overtime',
 * 			'data-on-text' => 'YES',
 * 			'data-off-text' => 'NO',
 * 			'data-label-text' => 'OT',
 * 		]
 * 	]) ?>
 *
 * @author Thanh Vinh <thanh.vinh@hotmail.com>
 * @version 1.0.0
 * @since 1.0.0
 */
class SwitchButton extends InputWidget
{
    public $checked = false;

    /**
     * Initializes the widget.
     */
    public function init()
    {
        parent::init();

        if ($this->options == null) {
            throw new InvalidConfigException("LiveFilter:: The 'options' property must be entered.");
        }
        SwitchButtonAsset::register($this->getView());
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        echo $this->renderInput();
        $this->registerAssets();
    }

    public function renderInput()
    {
        if ($this->hasModel()) {
            return Html::activeCheckbox($this->model, $this->attribute, $this->options);
        } else {
            return Html::checkbox(null, $this->checked, $this->clientOptions);
        }
    }

    /**
     * Registers the needed assets.
     */
    public function registerAssets()
    {
        $id = $this->options['id'];
        $js = "$('#{$id}').bootstrapSwitch();";

        $this->getView()->registerJs($js);
    }
}
