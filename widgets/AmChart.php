<?php

namespace ez\widgets;

use yii\web\View;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * @author Thanh Vinh <thanh.vinh@hotmail.com>
 * @since 1.0
 *
 * Example:
 * <?= AmChart::widget([
 * 	'chart' => [
 * 		'type' => 'serial',
 * 		'dataProvider' => [
 * 			['year'  => 2005, 'income' => 23.5],
 * 			['year' => 2006, 'income' => 26.2],
 * 			['year' => 2007, 'income' => 30.1]
 * 		],
 * 		'categoryField' => 'year',
 * 		'rotate' => true,
 *
 * 		'categoryAxis' => ['gridPosition' => 'start', 'axisColor' => '#DADADA'],
 * 		'valueAxes' => [['axisAlpha' => 0.2]],
 * 		'graphs' => [[
 * 			'type' => 'column',
 * 			'title' => 'Income',
 * 			'valueField' => 'income',
 * 			'lineAlpha' => 0,
 * 			'fillColors' => '#ADD981',
 * 			'fillAlphas' => 0.8,
 * 			'balloonText' => '[[title]] in [[category]]:<b>[[value]]</b>'
 * 		]]
 * 	],
 * ]); ?>
 */
class AmChart extends Widget
{
    /**
     * @var array the HTML attributes for the breadcrumb container tag.
     */
    public $options = [];

    /**
     * @var string the width of the chart
     */
    public $width = '640px';

    /**
     * @var string the height of the chart
     */
    public $height = '400px';

    /**
     * @var array the AmChart configuration array
     * @see http://docs.amcharts.com/3/javascriptcharts
     */
    public $chart = [];

    public function init()
    {
        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }
        AmChartAsset::register($this->getView());

        parent::init();
    }

    public function run()
    {
        $this->makeChart();
        $this->options['style'] = "width: {$this->width}; height: {$this->height};";
        echo Html::tag('div', '', $this->options);
    }

    protected function makeChart()
    {
        $chart = json_encode($this->chart);
        $id = $this->options['id'];
        $js = "AmCharts.makeChart('{$id}', {$chart});";
        $this->getView()->registerJs($js, View::POS_READY);
    }
}
