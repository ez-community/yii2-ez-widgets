<?php

namespace ez\widgets;

use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * Live search in a page content.
 * Usage:
 *  <?=	LiveFilter::widget([
 * 		'name' => 'filter',
 * 		'selection' => '.grid-view tbody tr',

 * 		'options' => [
 * 			'placeholder' => 'Search...',
 * 		]
 * 	]);	?>
 *  <?=	LiveFilter::widget([
 * 		'name' => 'filter',
 * 		'selection' => '.grid-view tbody tr',
 * 		'glyphIcon' => '.grid-view tbody tr',
 * 		'glyphIconAlignment' => 'left',
 * 		'options' => [
 * 			'placeholder' => 'Search...',
 * 		]
 * 	]);	?>

 * @author Thanh Vinh <thanh.vinh@hotmail.com>
 * @since 1.0
 */
class LiveFilter extends Widget
{
    public $name;
    public $selection;
    public $glyphIcon = 'glyphicon glyphicon-search';
    public $glyphIconAlignment = 'right';
    public $options = [];

    /**
     * Initializes the widget.
     */
    public function init()
    {
        parent::init();

        if ($this->name == null) {
            throw new InvalidConfigException("LiveFilter:: The 'name' property must be entered.");
        }
        if ($this->selection == null) {
            throw new InvalidConfigException("LiveFilter:: The 'selection' property must be entered, example: '.grid-view tbody tr'.");
        }
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        echo "\n" . '<div class="input-group">';

        if ($this->glyphIconAlignment == 'left') {
            echo "\n" . $this->renderGlyphIcon();
        }
        echo "\n" . $this->renderSearchInput();
        if ($this->glyphIconAlignment == 'right') {
            echo "\n" . $this->renderGlyphIcon();
        }
        echo "\n" . '</div>';

        $this->registerAssets();
    }

    /**
     * Renders the Glyph icon.
     * @return string the rendering result
     */
    protected function renderGlyphIcon()
    {
        if ($this->glyphIcon != null) {
            $content = Html::beginTag('span', ['class' => 'input-group-addon']);
            $content .= Html::tag('i', null, ['class' => $this->glyphIcon]);
            $content .= Html::endTag('span');

            return $content;
        } else {
            return null;
        }
    }

    /**
     * Renders the input text.
     * @return string the rendering result
     */
    protected function renderSearchInput()
    {
        return Html::input('search', null, null, [
                'id' => $this->name,
                'placeholder' => $this->options['placeholder'],
                'class' => 'form-control',
        ]);
    }

    /**
     * Registers the needed assets.
     */
    public function registerAssets()
    {
        $js = "$('#{$this->name}').keyup(function() {
            // Retrieve the input field text and reset the count to zero
            var filter = $(this).val();

            // Loop through the comment list
            $('{$this->selection}').each(function() {
                if ($(this).text().search(new RegExp(filter, 'i')) < 0) {
                    // If the list item does not contain the text phrase fade it out
                    $(this).fadeOut();
                } else {
                    // Show the list item if the phrase matches and increase the count by 1
                    $(this).show();
                }
            });
        })";

        $this->getView()->registerJs($js);
    }
}
