<?php

namespace ez\widgets;

/**
 * Live search in a page content.
 * Usage: the same as yii\bootstrap\Modal.
 * @author Thanh Vinh <thanh.vinh@hotmail.com>
 * @since 1.0
 */
class Modal extends \yii\bootstrap\Modal
{
    public $clearData = false;

    /**
     * Initializes the widget.
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        parent::run();

        if (isset($this->clearData) && $this->clearData) {
            $this->registerAssets();
        }
    }

    protected function registerPlugin($name)
    {
        //JS not rendered here
    }

    /**
     * Registers the needed assets.
     */
    public function registerAssets()
    {
        $js = "jQuery('#{$this->id}').on('hidden.bs.modal',function(){ $(this).removeData('bs.modal'); });";
        $this->getView()->registerJs($js);
    }
}
