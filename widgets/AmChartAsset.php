<?php

namespace ez\widgets;

use yii\web\AssetBundle;

/**
 * @author Thanh Vinh <thanh.vinh@hotmail.com>
 * @since 1.0
 */
class AmChartAsset extends AssetBundle
{
	public $basePath = '@vendor/thanh-vinh/yii2-ez-widgets';
	public $sourcePath = '@vendor/thanh-vinh/yii2-ez-widgets/assets/amcharts';
	public $css = [];
	public $js = [
		'amcharts.js',
		'funnel.js',
		'gauge.js',
		'pie.js',
		'radar.js',
		'serial.js',
		'xy.js',
	];
	public $depends = [
		'yii\web\JqueryAsset',
		'yii\bootstrap\BootstrapAsset',
	];
}
