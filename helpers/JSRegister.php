<?php

namespace ez\helpers;

/**
 * Register some boostrap scripts helper.
 * @author Thanh Vinh <thanh.vinh@hotmail.com>
 * @since 1.0
 */
class JSRegister
{
	/**
	* Boostrap tooltip.
	*
	* @param yii\base\View $view current view object
	* @param string $element element, class...
	*/
	public static function registerTooltip($view, $element)
	{
		$js = "$('{$element}').hover(function() {
			$(this).tooltip('show');
		})";
		$view->registerJs($js);
	}
	
	/**
	* Ajax submit for a form.
	*
	* @param yii\base\View $view current view object
	* @param string $form form id
	* @param string $element element id
	*/
	public static function registerAjaxSubmit($view, $form, $element)
	{
		$js = "jQuery('#{$element}').click(function() {
			$.ajax({
				type: 'POST',
				data: $('#{$form}').serializeArray(),
				success: function (data) {
					alert('abc');
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					alert('xyz');
				}
			});
		   return false;
		});";
		$view->registerJs($js);
	}
}
